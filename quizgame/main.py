#!/usr/bin/env python

__author__ = "Dennis Wagner"
__email__ = "dennis.wagner@th-brandenburg.de"


import sys
import os
from random import randint

from PyQt5 import QtGui, QtWidgets, QtCore
from PyQt5.uic import loadUi

class QuizWindow(QtWidgets.QWidget, QtCore.QThread):
    """"QuizWindow"""

    def __init__(self, parent=None):
        super(QuizWindow, self).__init__(parent)

        global paths
        paths = {"gui": "gui",
                 "icons": "icons"}

        # controller for widget switching
        # self.controller = controller
        self.initUI()

        self.questions = [{"question": "Die Mauer in Berlin wird niedergerissen",
                           "a": "1989" ,
                           "b": "1988",
                           "c": "1990",
                           "d": "1991",
                           "answer": "1989"},
                          {"question": "Japan attackiert Pearl Harbour",
                           "a": "1943",
                           "b": "1945",
                           "c": "1941",
                           "d": "1933",
                           "answer": "1941"},
                          {"question": "Wie heisst der Götterberg der Griechen?",
                           "a": "Olymp",
                           "b": "Ares",
                           "c": "Ceasar",
                           "d": "Marienberg",
                           "answer": "Olymp"},
                          {"question": "Nenne ein anderes Wort für Kältesteppen",
                           "a": "Wüste",
                           "b": "Tundra",
                           "c": "Meer",
                           "d": "Weide",
                           "answer": "Tundra"},
                          {"question": "Was ist die Muttersprache von Albert Einstein?",
                           "a": "Englisch",
                           "b": "Polnisch",
                           "c": "Französisch",
                           "d": "Deutsch",
                           "answer": "Deutsch"},
                          {"question": "In welchem Land liegt Vancouver?",
                           "a": "USA",
                           "b": "England",
                           "c": "Kanada",
                           "d": "China",
                           "answer": "Kanada"},
                          {"question": "Durch welche Stadt fliesst der Tiber?",
                           "a": "Rom",
                           "b": "Venedig",
                           "c": "Mailand",
                           "d": "Florenz",
                           "answer": "Rom"},
                          {"question": "In welcher Einheit gibt man die Leistung eines elektr. Gerätes an?",
                           "a": "Volt",
                           "b": "Ampere",
                           "c": "PS",
                           "d": "Watt",
                           "answer": "Watt"},
                          {"question": "Welcher Planet hat einen Ring?",
                           "a": "Saturn",
                           "b": "Mars",
                           "c": "Uranus",
                           "d": "Venus",
                           "answer": "Saturn"}
                          ]

        self.loadQuestion()

    def initUI(self):
        # load ui design from qt designer file
        loadUi(os.path.join(paths["gui"], str(self.__class__.__name__) + ".ui"), self)

        # set back button icon
        self.btnBack.setText("")
        # self.btnBack.setIcon(QtGui.QIcon(os.path.join(paths["icons"], "finish.png")))
        # self.btnBack.setIconSize(QtCore.QSize(buttonsize, buttonsize))

        self.btnAnswerA.clicked.connect(lambda: self.checkAnswer("a"))
        self.btnAnswerB.clicked.connect(lambda: self.checkAnswer("b"))
        self.btnAnswerC.clicked.connect(lambda: self.checkAnswer("c"))
        self.btnAnswerD.clicked.connect(lambda: self.checkAnswer("d"))

        self.btnBack.clicked.connect(lambda: sys.exit(self))

    def startQuestionTimer(self):
        # initialise and start question timer
        self.question_timer = QuestionTimerThread(self)
        self.question_timer.finished.connect(self.loadQuestion)
        self.question_timer.start()

    def loadQuestion(self):
        # enable buttons
        self.btnAnswerA.setEnabled(True)
        self.btnAnswerA.setChecked(False)
        self.btnAnswerB.setEnabled(True)
        self.btnAnswerB.setChecked(False)
        self.btnAnswerC.setEnabled(True)
        self.btnAnswerC.setChecked(False)
        self.btnAnswerD.setEnabled(True)
        self.btnAnswerD.setChecked(False)

        # random question
        self.question_number = randint(0, len(self.questions)-1)

        # set question and answer in gui
        self.setQuestion()

        # start question timer
        self.startQuestionTimer()


    def getQuestion(self, queastion_number):
        # TODO Datenbankabfrage -> Datenbank mit Fragen und Anworten generieren
        pass

    def setQuestion(self):
        self.lblQuestion.setText(self.questions[self.question_number]["question"])
        self.btnAnswerA.setText(self.questions[self.question_number]["a"])
        self.btnAnswerB.setText(self.questions[self.question_number]["b"])
        self.btnAnswerC.setText(self.questions[self.question_number]["c"])
        self.btnAnswerD.setText(self.questions[self.question_number]["d"])

    def checkAnswer(self, answer):
        # stop timer thread
        self.question_timer.stop()

        # set progress bar to zero
        self.progressBarAnswerTime.setValue(0)

        # disable buttons
        self.btnAnswerA.setEnabled(False)
        self.btnAnswerB.setEnabled(False)
        self.btnAnswerC.setEnabled(False)
        self.btnAnswerD.setEnabled(False)

        # get correct and user answer
        correct_answer = self.questions[self.question_number]["answer"]
        user_answer = self.questions[self.question_number][answer]

        # compare correct and user answer
        if correct_answer == user_answer:
            print("Die Antwort " , user_answer, " ist korrekt!")
        else:
            print("Die Antwort ", user_answer, " ist leider Falsch! Korrekt wäre ", correct_answer)

        self.sleep(3)
        self.question_timer.terminate()


class TimeError(QtWidgets.QMessageBox, QtCore.QThread):
    def __init__(self, controller, parent=None):
        super(TimeError, self).__init__(parent)

        self.controller = controller

        self.setIcon(QtWidgets.QMessageBox.Critical)
        self.setText("Please insert a patient name!".upper())
        self.setWindowTitle("Input Error")
        self.deleteMessageBox()
        #self.setStandardButtons(QtWidgets.QMessageBox.Ok)
        #self.standardButtons()
        # self.Ok.connect(lambda: self.controller.loadQuestion)
        self.exec_()

    def deleteMessageBox(self):
        self.sleep(4)
        self.close()
        self.controller.loadQuestion()


class QuestionTimerThread(QtCore.QThread):

    def __init__(self, controller):
        QtCore.QThread.__init__(self)
        self.controller = controller
        self.stop_flag = False

    def __del__(self):
        self.wait()

    def stop(self):
        self.stop_flag = True

    def run(self):
        # for i in range(1, 16):
        #     self.sleep(1)
        #     print(i)
        #     self.controller.progressBarAnswerTime.setValue(i)
        for i in range(1, 101):
            if self.stop_flag == False:
                # self.sleep(0.5)
                self.msleep(100)
                self.controller.progressBarAnswerTime.setValue(i)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    tApp = QuizWindow()
    tApp.show()
    sys.exit(app.exec())