#!/usr/bin/env python

__author__ = "Dennis Wagner"
__email__ = "dennis.wagner@th-brandenburg.de"

questions = [{"question": "Die Mauer in Berlin wird niedergerissen",
                   "a": 1989,
                   "b": 1988,
                   "c": 1990,
                   "d": 1991,
                   "answer": 1989},
                  {"question": "Japan attackiert Pearl Harbour",
                   "a": 1943,
                   "b": 1945,
                   "c": 1941,
                   "d": 1933,
                   "answer": 1941},
                  {"question": "Wie heisst der Götterberg der Griechen?",
                   "a": "Olymp",
                   "b": "Ares",
                   "c": "Ceasar",
                   "d": "Marienberg",
                   "answer": "Olymp"}
                  ]

print(questions[2]["answer"])
print(len(questions))